package zrpg.battle;

import java.util.ArrayList;

import zrpg.characters.Survivor;
import zrpg.characters.Zombie;

public class BattleSim {

	public final ArrayList<Zombie> zombies = new ArrayList<Zombie>();
	public final ArrayList<Survivor> survivors = new ArrayList<Survivor>();
	public final double ZOMBIE_STRENGTH = 1; //multiplication factor for zombie attack value
	
	public static void main(String[] args){
		
	}
	
	
	//returns true if the zombies win the battle
	//the array list of the surviving squad is updated with new current health
	public boolean simulateBattle(){
		double survivorsHealth = 0;
		double survivorsAttack = 0;
		double zombiesHealth = 0;
		double zombiesAttack = 0;
		
		for(int i=0; i < zombies.size(); ++i){
			zombiesHealth += zombies.get(i).getCurrHealth();
			zombiesAttack += zombies.get(i).getSelectedSkill().getAttackVal();
		}
		
		for(int i=0; i < survivors.size(); ++i){
			survivorsHealth += survivors.get(i).getCurrHealth();
			survivorsAttack += survivors.get(i).getSelectedWeapon().getAttackVal();
		}
		
		survivorsHealth = 200;
		survivorsAttack = 10;
		zombiesHealth = 100;
		zombiesAttack = 50;
		
		
		zombiesAttack *= ZOMBIE_STRENGTH; //adjust battle to adjust fairness
		
		double zombiesCurrHealth = zombiesHealth;
		double survivorsCurrHealth = survivorsHealth;
		boolean zombiesWin = false;
		int roundCount = 0;
		
		//iterate through rounds of the battle
		//priority is given to survivors in attacking first, resolves ties
		while(true){
			zombiesCurrHealth -= survivorsAttack;
			if(zombiesCurrHealth < 0){
				zombiesWin = false;
				break;
			}
			survivorsCurrHealth -= zombiesAttack;
			if(survivorsCurrHealth < 0){
				zombiesWin = true;
				break;	
			}
			roundCount++;
		}
			
		if(zombiesWin){
			double totalZombieDamage = survivorsAttack * roundCount;
			double zombieHit = totalZombieDamage / zombies.size();
			for(Zombie zombie : this.zombies){
				zombie.setCurrHealth(zombie.getCurrHealth() - zombieHit);
			}
		} else {
			double totalSurvivorDamage = zombiesAttack * roundCount;
			double survivorHit = totalSurvivorDamage / survivors.size();
			for(Survivor survivor : this.survivors){
				survivor.setCurrHealth(survivor.getCurrHealth() - survivorHit);
			}
		}
		
		return (zombiesWin);
	}
}
