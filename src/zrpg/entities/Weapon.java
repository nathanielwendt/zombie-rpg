package zrpg.entities;

public class Weapon extends Item {
	private int attackVal;
	private int crowdMult;
	
	public Weapon(int attack, int crowd, int genProb){
		super(genProb);
		this.attackVal = attack;
		this.crowdMult = crowd;
	}
	
	public int getAttackVal(){
		return this.attackVal;
	}
	
	public int getCrowdMult(){
		return this.crowdMult;
	}
}
