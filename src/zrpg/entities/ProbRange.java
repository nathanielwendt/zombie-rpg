package zrpg.entities;

public class ProbRange {
    private final double lowerBound;
    private final double upperBound;


    public ProbRange(double lowerBound, double upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    boolean includes(double givenValue) {
        return givenValue >= lowerBound && givenValue <= upperBound;

    }
}
