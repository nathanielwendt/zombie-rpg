package zrpg.entities;

import java.util.ArrayList;
import java.util.HashMap;
import zrpg.entities.ProbRange;

public class ItemGenerator {
	//Weapons
	public static final Weapon shovel = new Weapon(2,5,20);
	public static final Weapon axe = new Weapon(5,5,50);
	public static final Weapon baseballBat = new Weapon(4,6,40);
	public static final Weapon hammer = new Weapon(2,2,70);
	public static final Weapon pistol = new Weapon(8,7,10);
	public static final Weapon shotgun = new Weapon(10,10,2);
	
	//Skills
	public static final Skill swipe = new Skill(3,5,30);
	public static final Skill bite = new Skill(8,2,10);
	public static final Skill claw = new Skill(5,5,60);
	public static final Skill rush = new Skill(6,3,50);
	public static final Skill flail = new Skill(2,4,90);
	
	public ArrayList<Item> weapons = new ArrayList<Item>();
	public ArrayList<Item> skills = new ArrayList<Item>();

	public static void main(String[] args){
		ItemGenerator generator = new ItemGenerator();
		generator.weapons.add(shovel);
		generator.weapons.add(axe);
		generator.weapons.add(baseballBat);
		generator.weapons.add(hammer);
		generator.weapons.add(pistol);
		generator.weapons.add(shotgun);
		generator.populateWeaponProbs();
		
		generator.skills.add(swipe);
		generator.skills.add(bite);
		generator.skills.add(claw);
		generator.skills.add(rush);
		generator.skills.add(flail);
		generator.populateSkillProbs();
		
		String temp = generator.getWeaponFromProb(.99);
		String temp2 = generator.getSkillFromProb(.99);
		System.out.println(temp);
		System.out.println(temp2);

	}
	
	public void populateWeaponProbs(){
		int totalProb = 0;
		for(int i = 0; i < this.weapons.size(); ++i){
			totalProb += this.weapons.get(i).getGenProb();
		}
		
		double currentPosition = 0.0;
		for(int i = 0; i < this.weapons.size(); ++i){
			double itemInterval = (double) this.weapons.get(i).getGenProb() / (double) totalProb;
			this.weapons.get(i).rebuild(currentPosition, currentPosition + itemInterval);
			currentPosition += itemInterval;
		}		
	}
	
	public void populateSkillProbs(){
		int totalProb = 0;
		for(int i = 0; i < this.skills.size(); ++i){
			totalProb += this.skills.get(i).getGenProb();
		}
		
		double currentPosition = 0.0;
		for(int i = 0; i < this.skills.size(); ++i){
			double itemInterval = (double) this.skills.get(i).getGenProb() / (double) totalProb;
			this.skills.get(i).rebuild(currentPosition, currentPosition + itemInterval);
			currentPosition += itemInterval;
		}		
	}
	
	public String getWeaponFromProb(double searchProb){
		for(int i = 0; i < this.weapons.size(); ++i){
			if(this.weapons.get(i).getProbRange().includes(searchProb))
				return this.weapons.get(i).getId();
		}
		return null;
	}
	
	public String getSkillFromProb(double searchProb){
		for(int i = 0; i < this.skills.size(); ++i){
			if(this.skills.get(i).getProbRange().includes(searchProb))
				return this.skills.get(i).getId();
		}
		return null;
	}
	
	
}
