package zrpg.entities;

public class Item {
	public static int idCounter = 0;
	private String id;
	private int genProb;
	
	//internally used after the rebuild method is called, assigns them a specific probability within a histogram
	private ProbRange probRange;
	
	public Item(int genProb){
		this.id = Integer.toString(idCounter); //To-Do add real UUID generator here;
		this.genProb = genProb;
		idCounter++;
	}
	
	public String getId(){
		return this.id;
	}
	
	public int getGenProb(){
		return this.genProb;
	}
	
	public void rebuild(double lowRange, double highRange){
		this.probRange = new ProbRange(lowRange, highRange);
	}
	
	public ProbRange getProbRange(){
		return this.probRange;
	}
	
	
}
