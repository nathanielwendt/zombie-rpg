package zrpg.characters;

import java.util.ArrayList;
import zrpg.entities.*;

public class Zombie extends Character {
	private ArrayList<Skill> skills = new ArrayList<Skill>();
	private int skillIndex = -1;
	
	public Zombie(double currHealth, double maxHealth){
		super(currHealth,maxHealth);
	}
	
	public ArrayList<Skill> getSkills(){
		return this.skills;
	}
	
	public void selectSkill(String selectedSkillId){
		for(int i = 0; i < this.skills.size(); ++i){
			if(this.skills.get(i).getId().equals(selectedSkillId))
				this.skillIndex = i;
		}
	}
	
	public Skill getSelectedSkill(){
		return this.skills.get(this.skillIndex);
	}
	
}
