package zrpg.characters;

import java.util.ArrayList;
import zrpg.entities.Skill;
import zrpg.entities.Weapon;

public class Survivor extends Character {
	private ArrayList<Weapon> weapons = new ArrayList<Weapon>();
	private int weaponIndex = -1;
	
	public Survivor(double currHealth, double maxHealth){
		super(currHealth,maxHealth);
	}

	public ArrayList<Weapon> getSkills(){
		return this.weapons;
	}
	
	public void selectWeapon(String selectedSkillId){
		for(int i = 0; i < this.weapons.size(); ++i){
			if(this.weapons.get(i).getId().equals(selectedSkillId))
				this.weaponIndex = i;
		}
	}
	
	public Weapon getSelectedWeapon(){
		return this.weapons.get(this.weaponIndex);
	}
}
