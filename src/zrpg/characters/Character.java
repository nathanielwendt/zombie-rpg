package zrpg.characters;

import java.util.ArrayList;
import zrpg.entities.Skill;

public class Character {
	private String id;
	private double currHealth;
	private double maxHealth;
	
	public Character(double currHealth, double maxHealth){
		this.currHealth = currHealth;
		this.maxHealth = maxHealth;
	}

	public String getId(){
		return this.id;
	}
	
	public double getCurrHealth(){
		return this.currHealth;
	}
	
	public double getmaxHealth(){
		return this.maxHealth;
	}
	
	public void setCurrHealth(double currHealth){
		this.currHealth = currHealth;
	}
	
	public boolean isAlive(){
		return this.currHealth > 0;
	}
	
	public void refillHealth(){
		this.currHealth = this.maxHealth;
	}
}
