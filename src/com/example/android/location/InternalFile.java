package com.example.android.location;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import android.content.Context;

public class InternalFile {
	public static final String APP_META_FILE_NAME = "app_meta";
	public static HashMap<String,String> cachedAttributes = new HashMap<String,String>();
	public static boolean isCacheDirty = true;
	
	//writes an attribute to the app meta file
	//if successful, sets the cache to dirty since new data might be in the cache
	public static void writeAttribute(Context context, String key, String value){
    	FileOutputStream fos = null;
		try {
			fos = context.openFileOutput(APP_META_FILE_NAME, Context.MODE_PRIVATE);
			writeStringLine(fos,key + ":" + value);
			isCacheDirty = true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	//writes a string to a given fileoutputstream
	public static void writeStringLine(FileOutputStream fos, String line){
		try {
			fos.write(line.getBytes());
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	//Checks if the cache is up to date, if not it rebuilds it
	//then queries the cache for the given input key
	public static String getAttribute(Context context, String key){
		if(!isCacheDirty){
			return cachedAttributes.get(key);
		} else {
	    	//open the resulting file
	    	try {
				FileInputStream fis = context.openFileInput(APP_META_FILE_NAME);
		    	byte[] inputLength = new byte[(int) fis.getChannel().size()]; //casting long to int my change it's value, but since this won't be that large of a number it shouldn't matter
				try {
					int length = fis.read(inputLength);
					String input = new String(inputLength,0,length);
					String[] portions = input.split("\\r?\\n");
					for(int i = 0; i < portions.length; ++i){
						String[] split = portions[i].split(":");
						cachedAttributes.put(split[0],split[1]);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	
	    	//cache is now built and can be queried
	    	isCacheDirty = false;
	    	return cachedAttributes.get(key);
		}
	}
}
