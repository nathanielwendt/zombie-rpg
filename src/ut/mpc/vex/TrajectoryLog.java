package ut.mpc.vex;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;

public class TrajectoryLog {
	//public Context context;
	public static String fileName = "trajectory_file";
	
	public TrajectoryLog(){
	}
	
	public static void clearFile(Context context){
    	context.deleteFile(fileName);
	}
	
	//Writes each element of a trajectory to the file depcited by the fileName paramater of this class
	public static void writeToFile(Context context, Trajectory trajectory){	
    	FileOutputStream fos = null;
		try {
			fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		writeStringLine(fos,"Phenomenon Sensed >> ");
		writeStringLine(fos, trajectory.getPhenom().toStringStatic());
		writeStringLine(fos, "\n");
		int trajSize = trajectory.getSize();
		for(int i = 0; i < trajSize; i++){
			String entry = trajectory.toString(i);
			writeStringLine(fos,entry);
		}
		writeStringLine(fos,"--------");
		writeStringLine(fos,"Distance from start to end: ");
		writeStringLine(fos,Float.toString(trajectory.computeFinalVector()));
		
    	try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	public static void writeStringLine(FileOutputStream fos, String line){
		try {
			fos.write(line.getBytes());
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	//displays the file to the console
	public static void displayFile(Context context){   	
    	//open the resulting file

    	try {
			FileInputStream fis = context.openFileInput(fileName);
	    	byte[] input = new byte[(int) fis.getChannel().size()]; //casting long to int my change it's value, but since this won't be that large of a number it shouldn't matter
			try {
				int length = fis.read(input);
				String s = new String(input,0,length);
				System.out.println(s);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
}
