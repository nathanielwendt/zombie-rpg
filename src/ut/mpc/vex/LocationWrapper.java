package ut.mpc.vex;

import android.location.Location;

public class LocationWrapper {
	public double longitude;
	public double latitude;
	public Location androidLoc; //for Android Wrapper
	
	//@standard constructor for coordinate based construction
	public LocationWrapper(double longitude, double latitude){
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	/* -------------------------------
	 * Android Wrapper Characteristics     
	 */
	
	//@Use this constructor when using the android class - Location
	public LocationWrapper(Location androidLoc){
		this.locationObjToLocationWrapper(androidLoc);
	}
	
	//@For Android location object conversion to raw longitude and latitude
	//Populates the latitude and longitude of a
	public void locationObjToLocationWrapper(Location loc){
		this.androidLoc = loc;
		this.longitude = loc.getLongitude();
		this.latitude = loc.getLatitude();
	}
	
	//@computed distance is stored in results[0].  If results has length 2 or greater, the initial bearing is stored in results[1].  If
	//@results has length 3 or greater, the final bearing is stored in results[2].
	public void distanceTo(LocationWrapper dest, float[] results){
		Location.distanceBetween(this.latitude, this.longitude, dest.latitude, dest.longitude, results);
	}
	
	public String toString(){
		String result;
		result = String.valueOf(this.longitude);
		result += ", ";
		result += String.valueOf(this.latitude);
		result += "\n";
		return result;
	}
	
	public void printCoordinates(){
		System.out.println("Longitude: " + this.longitude);
		System.out.println("Latitude: " + this.latitude);
	}

}
