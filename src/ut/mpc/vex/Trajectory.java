package ut.mpc.vex;

import java.io.FileNotFoundException;
import ut.mpc.phenom.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

import android.content.Context;

public class Trajectory {
	ArrayList<LocationWrapper> locations = new ArrayList<LocationWrapper>();
	Phenom phenom;

	public Trajectory(Phenom phenom){
		this.phenom = phenom;
	}

	public void addLocation(LocationWrapper loc){
		this.locations.add(loc);
	}
	
	public Phenom getPhenom(){
		return this.phenom;
	}
	
	public String toString(){
		String result = "*\n";
		for(int i = 0; i < this.locations.size(); i++){
			result += this.locations.get(i).toString();
			result += "-------------------------------\n";
		}
		result += "**** </Trajectory> ***";
		return result;
	}
	
	//prints a given entry of the trajectory
	//if position is out of bounds, will return "-1"
	public String toString(int pos){
		if(pos < this.locations.size())
			return this.locations.get(pos).toString();
		else
			return "-1";
	}
	
	public int getSize(){
		return this.locations.size();
	}
	
	public void printLocations(){
		for(int i = 0; i < locations.size(); i++)
			locations.get(i).printCoordinates();
	}
	
	public float computeFinalVector(){
		LocationWrapper locStart = locations.get(0);
		LocationWrapper locEnd = locations.get(locations.size() - 1);
		
		float[] results = new float[3];
		locStart.distanceTo(locEnd, results);
		return results[0];
	}
	
	public float getBearingTo(){
		LocationWrapper locStart = locations.get(0);
		LocationWrapper locEnd = locations.get(locations.size() - 1);
		float[] results = new float[3];
		locStart.distanceTo(locEnd, results);
		return results[1];
	}
}
