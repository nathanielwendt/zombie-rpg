package ut.mpc.vex;

import android.widget.TextView;

public class TrajectoryProcessor {
	
	//currently process will print for debugging
	//To-Do change this to update view objects on device
	public static void process(TextView textv, Trajectory traj){
		String output = "";
		output += "phenom is: " + traj.getPhenom().toStringStatic() + ", ";
		output += "bearingTo: " + traj.getBearingTo() + ", ";
		output += "dist: " + traj.computeFinalVector() + "\n";
		textv.setText(output);
	}
}
